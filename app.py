"""
used for sending emails using celery task
"""
import os
import sqlite3
import datetime
import time

from flask import Flask, request
from flask_restful import Resource, Api
from flask_mail import Mail, Message
from celery import Celery

# all configurations
# #TODO: later will move this into config file
app = Flask(__name__)
app.config.update(SECRET_KEY=os.urandom(24))

api = Api(app)

# for mails
# add your MAIL_USERNAME and MAIL_PASSWORD (i.e gmail account)
app.config.update(
    DEBUG=True,
    # EMAIL SETTINGS
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME='',
    MAIL_PASSWORD='',
    MAIL_DEFAULT_SENDER='no-reply@gmail.com')
mail = Mail(app)

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379'

# for celery task
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def database_opretaion(data=None, params=None):
    con = sqlite3.connect('test.db')

    cursorObj = con.cursor()

    cursorObj.execute("""
        CREATE TABLE IF NOT EXISTS Emails(
        task_id TEXT PRIMARY KEY,
        email_id TEXT NOT NULL,
        body TEXT NOT NULL,
        send_timestamp timestamp NOT NULL,
        email_status Text)""")

    con.commit()

    if data:

        unix = int(time.time())
        send_timestamp = str(
            datetime.datetime.fromtimestamp(unix).strftime(
                '%Y-%m-%d %H:%M:%S'))

        cursorObj.execute("""INSERT INTO Emails VALUES(?, ?, ?, ?, ?)""",
                          (data['task_id'], data['recipient_email_id'],
                           data['email_body'], send_timestamp,
                              data['email_status']))

        con.commit()

        return send_timestamp

    if params:
        from_date = params['from_date']
        to_date = params['to_date']

        cursorObj.execute(
            """SELECT * FROM Emails WHERE (send_timestamp >= ? AND send_timestamp < ?)""",
            (from_date, to_date))

        rows = cursorObj.fetchall()

        return rows

    con.close()


# Added celery decorator to send_mail function
@celery.task
def send_mail(data):
    """
    used for sending email using celery task.
    """
    msg = Message(data['subject'],
                  sender=app.config['MAIL_DEFAULT_SENDER'],
                  recipients=[data['to']])
    msg.body = data['body']

    with app.app_context():
        mail.send(msg)


class EmailAPI(Resource):
    def get(self):
        """
        Note:
        # #TODO: if we want to update the current_status of the task_id then
        we can do that using celery.AsyncResult(task_id).
        """

        params = {
            'from_date': request.args['from_date'],
            'to_date': request.args['to_date']
        }

        # # get the details and update the details in table
        # result = celery.AsyncResult(task_id)

        # #TODO: here if we want to update the status in db then update the row

        data = database_opretaion(data=None, params=params)

        response_list = []
        for row in data:
            response = {}
            response['task_id'] = row[0]
            response['email_id'] = row[1]
            response['body'] = row[2]
            response['send_timestamp'] = row[3]
            response['email_status'] = row[4]
            response_list.append(response)

        return response_list

    def post(self):
        """
        used for sending email
        """
        response_data = {}

        # json request
        json_input = request.json

        recipient_email_id = json_input.get('email', None)
        email_body = json_input.get('body', None)

        email_data = {
            'subject': 'Mail Regarding Interview Task',
            'to': recipient_email_id,
            'body': email_body
        }

        # call function for sending email
        response = send_mail.apply_async(args=[email_data])
        if response:
            task_id = response.task_id
            email_status = response.status

            data = {
                'task_id': task_id,
                'email_status': email_status,
                'recipient_email_id': recipient_email_id,
                'email_body': email_body
            }
            send_timestamp = database_opretaion(data=data, params=None)
            response_data = {
                'task_id': task_id,
                'email_status': email_status,
                'send_timestamp': send_timestamp
            }

        return response_data


# route for get and post
api.add_resource(EmailAPI, '/email/',
                 methods=['POST'], endpoint='emailpostapi')
api.add_resource(EmailAPI, '/email/',
                 methods=['GET'])

# initializing app
if __name__ == "__main__":
    app.run(debug=True)
