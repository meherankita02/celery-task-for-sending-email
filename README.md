# Celery Task For sending Email

send email using celery task.


# README #

### Intro ###

### Contributing Guidelines ###

TODO: Add more

### Coding Guidelines ###

* Keep your code readable
* Follow pep8 conventions, including but not limited to the following:
    1. 80 character lines limit
    2. double newlines between classes, and functions
    3. single newlines between methods of classes
* Futher reading here: https://docs.google.com/document/d/1maGMJBjeiC-bJ5r5hRKen85a8LGujRKcCH8-J2fBNGw/edit

### Local Development and Setup ###

#### External Dependencies ####

#### Python Dependencies and application run ####

1. install virtualenv: https://virtualenv.pypa.io/en/latest/installation/
2. create virtualenv with python3 (your system might have python3 as python):
    ```
    python3 -m pip install --user virtualenv
    python3 -m venv env

    ```
3. enable virtualenv:
    ```
    . venv/bin/activate or source env/bin/activate
    ```
4. install python dependencies:
    ```
    pip install -r requirements.txt
    ```
5. run the application:
    ```
    python app.py
    ```
6. Leaving the virtual environment¶
    ```
    deactivate
    ```

#### Install Notes ####

1. install sqlite3 databse
    ```
    pip install pysqlite
    ```
2. install celery task (for email sending background task)
    ```
    pip install celery
    ```
3. install radis server for more (information https://tableplus.com/blog/2018/10/how-to-start-stop-restart-redis.htmlhttps://tableplus.com/blog/2018/10/how-to-start-stop-restart-redis.html)
    ```
    1] pip install redis or brew install redis
    2] brew services start redis

    ```




